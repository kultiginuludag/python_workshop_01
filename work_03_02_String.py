# Stringler iterable obje dir.

mystr = "İstanbul"

#print(mystr[0] )
#print(mystr[3] )
#print(mystr[7] )

# Slice method : string, tuble, list syntax [start : stop : step ]
"""
mystr = mystr[1:3]
print(mystr)

str_01 = mystr[::-1]
print(str_01)

print(mystr)
"""

# split ayırma methodu split(" * ")
mystr = " hello word I   came back again ! "
print(mystr.split(" "))

# upper lower method
mystr= "iStANBuL"

print(mystr.lower())
print(mystr.upper())

# isupper, islower

y = "ANKARa".islower()
print(y)

y="ankara5".isalnum()
print(y)

# find  ----> bulduğu indexi yazar
mystr = "Korkma şafak sönmez bu şafaklarda yüzen al sancak!"
print(mystr.find("şafak"))

#strip, baştaki ve sondaki boşlukları trim eder

mystr= "   my number is seven  "
print(mystr.strip())


# concatenate, format method, join method

str_1="ali"
str_2="veli"
str_3  = str_1 + str_2
print(str_3)

a_1 = "selam"
a_2 = "dünya"
a_3 = "Hoşçakalın"

print("{}, insanlar mutlu yaşa {}da \n {} behlül kaçar".format(a_1, a_2,a_3))

# Joinb method, bir itereable obke üzerinde sırayla birleştirir

mylist = ["ali","veli","ayşe"]

print("****".join(mylist))

