""" WHILE
while <şartım> doğru ise;
    <code block>  bunu yap
    <şartım> güncelle ( güncellemek zorunlu değil sonsuz döngü olur)

    <şartım> yanlış ise --->Çıkış yap

"""

"""
x =10
while 0<x:
    print(f" {x} . kontrol")
    x -=1
    """


"""
while "selam":
    print("sonsuz döngüye girer")
    """

# task01: verilen bir sayı için, 1 den büyük ve 3 e tam bölünen sayıların toplamı ? sayıyı konsoldan istesin
"""
sayı = int (input(" sayı girin : "))

a = 1
toplamım = 0

# 7 girilsin [1,2,3,4,5,6]
while a < sayı :
    a +=1

    if a % 3 :      # 2 % 3 ---> 2
        continue


    toplamım +=a

print(f" 3 e tam bölünen sayıların toplamı : {toplamım}")

            """
"""
sayı = int (input(" sayı girin : "))

a = 1
toplamım = 0

# 7 girilsin [1,2,3,4,5,6]
while a < sayı :

    print(" sonsuz loop a girdi")
    if a % 3 :      # 2 % 3 ---> 2
        continue

    toplamım +=a

    a += 1

print(f" 3 e tam bölünen sayıların toplamı : {toplamım}")

"""
# break  -----> break gördüğü yerde kodu kırar, loop kırıp dışına çıkar


# task_02: bana verdiğin sayı asal sayımı değilmi bunu kontrol eden program yaz
# bir sayı yarısına kadar olan sayılar teker teker bölündüğünde her defasında kalan varsa, bu sayı asal sayıdır.
"""
10 a bakalım; yarısı 5 ,  5 ten büyük hiçbir sayı 10 u tam bölmez, o zaman ben 5 e kadar olan sayıları
kontrol etmem yeterli.
"""

"""
is_Prime = True
mynum= int(input("Bir sayı giriniz : "))

div = 2  # en küçük asal sayı 2

while div <= int(mynum/2):
    if not mynum % div :
        is_Prime = False

        break
    div +=1


if is_Prime:
    print(f" mynum is Prime :  {is_Prime}")
else:
    print(f" mynum is Prime : {is_Prime}")
            """

"""
while <True> :
    some code
    while doğru oldu müddetce içindeki kodu execute ediyor
    while folse olunca loop kırıldı, ama false durumundaa da şunu yap dese ???
    
else:
    <loop kırılınca bunu yap>

"""

"""
x = 1
while x < 5:
     print(x)
     x += 1
else:
     print("Loop was stopped at:")
     print(x)
            """

# FOR LOOP
""" for <şartım> : doğru ise içeri gir
        <code block>
        <şartı> kontrol et
        
eğer <şart> false ise Loop dan çık

"""

#range fonksiyonu
"""
x = range(5)  # iterable obje oluşturuyor 5 e kadar (5 dahil değil) 0 dan başlayarak rakamları tutuyor  0,1,2,3,4

y = list(x)
print(y)

            """
"""
for x in range(5):
    print (x)

"""
"""

mylist = [5,8,12,"ali", "veli", 10]
a = 0
for i in mylist:
    
    a += 1
    print(f" Listedeki {a}. elemanınm {i}")

"""
"""
# bana bir kelime verilsin, ben bu kelimenin uzunluğu kadar bu kelimeyi sayayım
# örnek "ali" 3 harfli len("ali") = 3
# program : 1 ali, 2 ali , 3 ali yazsın

mystr = input ("bir kelime girin : ")

harfsayisi = len(mystr)

print(harfsayisi)

for i in range(harfsayisi) :
    print(f" {i+1} , {mystr}")
                """





