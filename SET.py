# A set is a collection mutable and dynamic, unordered , duplication not allowed
myset_01=set([1,2,3,4,5])
print(myset_01)

#2. yol
my_set = {1,2,3,3,3,4}
print(type(my_set))
print(my_set)

#boşküme
myset_02 = set()
print(myset_02)
my_empty_set = {}
print(f"boş kümem : {my_empty_set}")
print(type(my_empty_set))

#NOT: BOŞ KÜME küme={} şeklinde create edemezsin bu dictionary olur
#Boş kümeyi sadece = set() şeklinde create edersin

myset_00 = set("ankara")
print(myset_00)


# UNION
s1 = {"a","b","c"}
s2={1,2,"a","c"}
s3={"d","b",5}
s4 = {"nadide","sefa","irem",}
s5={"naz","pinar","kültigin"}

myunion = s1|s2|s3
print(f" birleşim kümem: {myunion}")

myunion_02 = s1.union(s2,s3,s4,s5)
print(myunion_02)

#Intersectıon KESİŞİM KÜMESİ

my_inter=s1 & s3
print(my_inter)

my_inter_01=s1.intersection(s2)
print(my_inter_01)