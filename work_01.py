# input function
"""
x = int(input("bir sayı giriniz : "))

print(f"ekrandan aldığım x değeri : {x}")

#y = input("bir şehir giriniz : ")"""

#print(f" x değerinin type : { type(x)}")

# print(f" y değerinin type : { type(y)}")

# input  ile aldığım herşey string e dönüyor
""""
print(x+5)
"""

""" Boolean ve Karşılaştırma """

# bir ifade ya True yada False
# p or q, p and q, not p ----> p nin değil

logi_exp_01 = 5<10 #------> logical bir gösterim : Bool

print(logi_exp_01)

logi_exp_02 = isinstance(5.7, int)

print(logi_exp_02)

logi_03 = 7 == 7.0
print (logi_03)

logi_04 = not 7 !=7.0  and 5<10   # True  and True  ---> True  ( * ) ----> True = 1 , False = 0
print(logi_04)

logi_05 = 7!=7.0  or 5<10   #  False or True ----> True (+)  ----> 0 + 1 == 1

print(f" logi_05 değeri : {logi_05}")

