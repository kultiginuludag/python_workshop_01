# declaration

x = 7
y = 8

c = x+y

print(c)

d = x -y
print(d)

e = x*y

print(e)
print(f"x ile y nin çarpımı = {e}")

f = x/y
print(f"x bolu y = {f}")

print(f"x bolu y direk = {x/y}")


#Integers are also class object. that is integer is a class
# Integers are  Immutable Objets like float, string and tuple

# we use  id() to determine objets identity

num = 5

print(f" num sayımın id si = {id(num)}")

num = num +5

print(f" num sayısının işel sonrasındaki id si {id(num)}")

""" sonuçta variable adı aynu num ,
başta = 5 iken başka bi obje,
işlem yaptıktan sonra num = 10 artık bu başka bi obje.
"""

# çak büyük sayıları okumak için _ kullanırız

x = 123456789
y = 123_456_789
print(f" x = {x}")
print(f" y = {y}")

print(f" x y değeri eşit mi ? = {x==y}")

""" x//y  ve x%y """

print(f" x/y  = {49/5}")
print(f" x//y  = {49//5}") # bölme işleminin tam kısmını verir
print(f" x%y  = {47%5}")  # a%b a sayısının b ile bölümünden kalan === a mod(b)


# Increment and decrement
x = 10

x += 5
print(x)

x -= 10
print(x)

x /=5
print(x)

x *= 25
print(x)

x %= 7
print(x)

x **=3    # a ** b a sayısnın b inci kuvveti yani 3**4 = 3x3x3x3 = 81
print(x)

x //=10

print(x)

""" Bir objenin tipi type() ( yani int, float, string vs) 
 
 ve bir objenin bir class'in objesimi (instance mı ?)  isinstance()"""

mynum = 50
print(f" mynum elemanımın tipi nedir ? {type(mynum)}")

print(f" mynum int class bir objesi mi? bir instance mı ? = {isinstance(mynum, int)}")
print(f" mynum str class bir objesi mi? bir instance mı ? = {isinstance(mynum, str)}")


# " selam saat 5"
# mystr= "5"     int(mystr)=5

# convert int to string vice versa

a = 10
print(type(a))

b = str(a)

print(f"b nin data tipi = {type(b)}")

"""# comperation < küçüktür, <=  küçük ve eşit , > büyük, >= büyük ve eşit, == eşitt
!= eşit değil, 

lagical operatorler, "and", "or" , "not"

member ship: "in" , "not in"
"""
print( 5 not in [1,2,3,4]) # sonuç false neden 5 sayısı list içinde yok
print( f" 5 ve 7 listin içinde değil [1,2,3,4] = { 5 and 7 not in [1,2,3,4]}")

""" FLOAT  5,45 89,13 """

x = 7.81
print(type(x))

import math

x = math.sqrt(16)
print(x)

print(math.dist((5,7),(8,7.93255)))
print(math.log(9,3))


import random

""" random.randint(a,b)
a ve be dahil olmak üzere bu aralıkta bana rastgele sayılar üretir"""

print ( random.randint(3,33))

"""random.choice (sequence) . verilen seri içerisinde ( bu liste olabilir) , v.s  eleman seçer"""

print(random.choice(["ali", "veli", "ayşe",5,9.8]))

""" 0 ile 1 arasında rastgele reel sayılara ihtiyacım var random.random()"""

print(random.random())


""" random.shuffle verilen bir sequence (liste olabilir) sıralamasını yani order karıştırır"""
a = ["ali", "veli","ayşe","fatma","zeynep"]

b = (1,2,3,4,5,6) # tuple değiştirilemez yani item assignement yoktur o yüzden shuffle tuple de çalışmaz

random.shuffle(a)
print (f"shuffle çalışsın = {a}")













