
# fonksiyon başka bir fork arguman olarak alabilir ( passing functions as arguments )
# benim elimde bir fonksiyon var ,ben yenibir fonk yazıyorum, bu yeni fonk önce fonksiyonu alacak
#arguman olarak yazdıklarımı bu eski fonksiyonda tek tek hesaplayıp liste basacak

def square (x):
    return x**2
def cupe (x):
    return x**3
def mynew_func(func, *args):

    result=[]
    for i in args:
        result.append(func(i))
    return  result

print(mynew_func(square, square(1),square(2),square(0)))

print(mynew_func(cupe, 0,3,5))


#Storing Func in data structures, fonksiyonlarımı ben bir dict tuta bilirim

myfunc_data= {"kare":square, "küp":cupe,"yeni":mynew_func}


def get_func(func_name):
    return myfunc_data.get(func_name)

new = get_func("yeni")
print(new(square,0,1,2))


#Python build in function

abs()